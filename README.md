# jpoint21-reactive-blocks
Цель: отработать на практике работу с виртуальными потоками в java21 со spring-boot 3.2. 
За основу взят проект https://gitlab.com/AlexanderMv/jpoint-reactive-blocks

Чтобы использовать виртуальные потоки, нужно: 
1. добавить настроку в application.properties: 
```
spring.threads.virtual.enabled=true
```
2. добавить аргумент:
```
-Dreactor.schedulers.defaultBoundedElasticOnVirtualThreads=true
```

Теперь можно использовать виртуальные потоки. 

Методы аннотированные @Async используют виртуальные потоки.

Чтобы не блокировать обработку http запросов, обрабатывая долгие запросы, нужно использовать .subscribeOn(Schedulers.boundedElastic()). 
Таким образом долгий запрос уходит в виртуальный поток.
Пример использования в /data/{value}
