package org.jpoint21reactiveblocks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class LongProviderService {

    private static final Logger log = LoggerFactory.getLogger(LongProviderService.class);
    public long get(long value) {
        log.info("Async task method has been called {}", Thread.currentThread());
        log.info("get request, value {}", value);
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(value)); //Блокирующий вызов найден с помощью blockhound
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("get request, value {}, done ", value);
        return value;
    }
}
