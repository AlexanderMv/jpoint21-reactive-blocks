package org.jpoint21reactiveblocks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
public class MyController {

    private final LongProviderAsyncService longProviderAsyncService;
    private final LongProviderService longProviderService;

    private static final Logger log = LoggerFactory.getLogger(MyController.class);

    public MyController(
            LongProviderAsyncService longProviderAsyncService,
            LongProviderService longProviderService
    ) {
        this.longProviderAsyncService = longProviderAsyncService;
        this.longProviderService = longProviderService;
    }

    /**
     * Вызов долгого процесса.
     * @param value - время выполнения долгого асинхронного процесса с блокировкой в секундах
     * @return время выполнения. Например: 100
     */
    @GetMapping("/data/async/{value}")
    public Mono<String> dataAsync(@PathVariable long value){
        log.info("Rest controller method has been called {}", Thread.currentThread());
        //Использует виртуальные потоки. Например: VirtualThread[#304688,task-304613]/runnable@ForkJoinPool-1-worker-4
        longProviderAsyncService.get(value);
        return Mono.fromCallable(()->String.format("%d%n", 123));

    }

    /**
     * Вызов долгого процесса. Использует виртуальные потоки.
     * При запуске использовать аргумент: -Dreactor.schedulers.defaultBoundedElasticOnVirtualThreads=true
     * В конфиге указать: spring.threads.virtual.enabled=true
     * @param value - время выполнения долгого блокирующего процесса в секундах
     * @return время выполнения. Например: 100
     */
    @GetMapping("/data/{value}")
    public Mono<String> data(@PathVariable long value){
        log.info("Rest controller method has been called {}", Thread.currentThread());
        return Mono.fromCallable(()->String.format("%d%n", longProviderService.get(value))).subscribeOn(Schedulers.boundedElastic());

    }

    /**
     * Вызов быстрого процесса.
     * @param value - значение которое вернет ответ
     * @return значение из параметра value
     */
    @GetMapping("/dataReact/{value}")
    public Mono<String> dataReact(@PathVariable long value){
        log.info("Rest controller method has been called {}", Thread.currentThread());
        return Mono.fromCallable(()->String.format("%d%n", value));
    }
}
